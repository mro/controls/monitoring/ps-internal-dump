# TimWeb Tools
> In order to use both of the tools described bellow, the following actions need to take place:
>   * Connect in RDP to: cerntstim.cern.ch
>   * Navigate to "TIM" folder where both applications reside.
>

***Guidelines***

Guidelines for both of the tools can be found in the following presentation:
https://edms.cern.ch/ui/file/1219753/1/TIM_training.pdf

## Tim Dashboard Editor
> The TIM Dashboard Editor is a customized version of the standard IBM ILOG Dashboard Editor which allows you to draw dashboard diagrams and to define which TIM DataTags that shall be used to animate the symbols.
> More info on the editor: https://readthedocs.web.cern.ch/display/TIM/TIM+Dashboard+Editor

Each creation through this application is called a "View", these views are stored in SVN under the following folders:

>   * tim-views-next
>   * tim-views-pro
>   * tim-views-old

For every new view we create we initially store it under the tim-views-next folder.

**Creating a view** 

For the purposes of the PS-ID internal dump application we shall use an SVG file containing the full design of the mechanism plus an SVG file with the representation of the core.

***Tips and Steps***

After opening the desired SVG, by clicking on Visibility -> Link with DataTag we can manipulate the system name with a parameter in order to map it.

If we click Preview -> Edit -> Simulation mode, by changing the value there we can observe the change of behaviour of our item according to the value.

Associate the parameters of the class with parameters of the symbol and in this way we get the animation for the entire symbol.

Keep in mind that classes need to be configured before we get to use them!

***Linking an item***

Directly on the view we can choose for example to animate text, so we choose text animation -> link with DataTag, find the desired tag and confirm.

In property sheet we can edit the look of the created item/text.

If we want to set a certain behaviour or reuse an item several times we should rather create a symbol in TIM Symbol Creator.
These symbols can be accessed through palettes on the right side of the Dashboard editor.

***Saving the view***

File -> save -> tim-views-next then make our own folder under test.

From next we can view our creation in tim viewer but not tim web viewer. In tim web viewer we can only see views saved in tim-views-pro.

Save as Dashboard File (editable/preferred)

## Tim Symbol Editor

***Creating a Symbol***

Create Symbol -> press preview, edit values to define the modification on the symbol depending on the input.

In this way we "play" with our symbol and test its behaviour before we actually use it in Dashboard Editor and link it.
For testing purposes we store our symbols locally and then in Dashboard editor we choose: load a palette.

Symbol -> save as -> This PC -> users -> name -> tim-dashboards-palettes and then we save our .jar file.

We can normally use our palette without committing it but it's only available in our view, as long as we work in the same machine. Otherwise we need to commit our palette.

***More on the Editor***

Every Symbol has a base element, it's the element that is later on fixed and all the calculations are made with respect to the base element.

Symbol outline -> Parameters -> set name -> set default value -> set type of value

***Designing a Symbol***

We design our symbols and then we navigate to parameters at the left side of the panel. There, we create a parameter.

In Styling Customizer at the bottom of the editor we set a name and a default value (i.e sensor name). 
We also need to set the type of the value.
Afterwards, we define the color of the symbol.

We load a palette named "standard-symbol" from which we copy all the parameters which are the standard parameters that we shall also use for our symbol.
So we copy them into our symbol's parameters.

In these parameters we have quality color, which is by default green so we use this color as default when everything is normal and then we can add a threshold behaviour.
So, we add a new parameter called threshold which will be the defined threshold according to which we change color.

! Sidenote: The order of the parameters doesn't matter. Just copying the default parameters to our symbol's parameters is sufficient.


Define the borders of our symbol with a rectangle for example and have it as a base.
We shall not perform any actions on it as it's just used to define our design's limits.

If we want our subelements to be consistent with our base element in terms of size and position, then for each element in Styling Customizer we choose transform and make sure we choose "proportional to base".

To proceed with adding a text box we click whenever we need to place it, making sure we tick proportional to base again for this item.
While in styling customizer, on text display we can choose antialiasing. 
Then we want to bind that text to the name parameter we set earlier.

In Styling Customizer -> Text -> Click -> Enter an Expression and in the text area we now have available all our Parameters from which we will choose the name.

Then, if we click preview and play around with the name we will see the changes being displayed accordingly on our symbol.

For our text we need to also choose center relative to base element and in the text tab Resizing mode -> Zoom only.
In this way we are able to zoom in our text.

After we are finished with our text and its behaviour we can copy this element and by this action we copy its behaviour as well.
We just need to make sure to bind it to the right parameter.

If we nee to add a symbol like a measurement unit, in Styling customizer -> Text -> Label we can format the decimal.
In this way we know that our added symbol will always have the same size as the value.

***Colors***

Styling Customizer -> Paint -> Fill paint, click enter an expression and choose quality color.

Quality color is animated by TIM itself and by default if any of the data that we map to our parameters is unknown because of a wrong mapping, the default would be purple which indicates that something is wrong with our symbol and we should check our mapping.
If everything is ok, then the standard is green.
If there is any problem with the communication then the color would become blue.

! Tip: Use the tile colors according to the rules above in order to detect issues with our communication and the value box can change color according to our preferred threshold.

***Setting Threshold***

Moving to our next symbol we choose "new condition".

We choose ie. temperature - is greater than - threshold.

Then in Styling Customizer we modify our condition.

Fill paint the color to which we will change according to our condition.
