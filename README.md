# PS Internal Dump Active Monitoring System

> A web based monitoring interface to allow the dumps monitoring during operation and machine development.

## PS Internal Dump Sensors.

**Both Dump 47 and Dump 48**  

13 temperature sensors each:  
``` 
- TWDin: water temperature inlet on the dump head circuit.
    Acquisition#temperatureWaterDumpIn
- TWDout: water temperature outlet on the dump head circuit.
    Acquisition#temperatureWaterDumpOut
- TWCin: water temperature inlet on the vacuum chamber circuit. 
    Acquisition#temperatureWaterChamber1
- TWCout: water temperature outlet on the vacuum chamber circuit.
    Acquisition#temperatureWaterChamber2
- TC1: dump core graphite temperature on the right of the beam impact.
    Acquisition#temperatureGraphiteCore1
- TC2: dump core graphite temperature on the left of the beam impact.
    Acquisition#temperatureGraphiteCore2
- TCu1: dump core CuCrZr temperature on the right of the beam impact.
    Acquisition#temperatureCuCrZrCore1
- TCu2: dump core CuCrZr temperature on the left of the beam impact.
    Acquisition#temperatureCuCrZrCore2
- TE1 and TE2: temperature of the electro-magnet coils (thermo-switches).   
    ExpertAcquisition#thermoSwitch1Active /  ExpertAcquisition#thermoSwitch2Active
- TM: temperature on the electro-magnet coils (temperature sensor).
    Acquisition#temperatureMagnet
- TVC1: temperature on the vacuum chamber #1 (DN350 flange, next to mechanism).
    Acquisition#temperatureVacuumChamber1
- TVC2: temperature on the vacuum chamber #2 (downstream vacuum flange).
    Acquisition#temperatureVacuumChamber2
```

8 position sensors each (6 for dump position + 2 for safety motor):  
``` 
- DSout 1 and 2: Two dump switches1, position OUT (located inside the mechanical stops): active when the dump is in parking position.
    ExpertAcquisition#sensorDumpSwitchOut1 / ExpertAcquisition#sensorDumpSwitchOut2 (ExpertAcquisition#sensorDumpSwitchOut1Ok / ExpertAcquisition#sensorDumpSwitchOut2Ok)
- DSin: dump switch position IN (located inside the mechanical stops): active when the dump is in beam position.
    ExpertAcquisition#sensorDumpSwitchIn1 / ExpertAcquisition#sensorDumpSwitchIn2 + Ok
- DSup: dump switch position IN (located on the top part of the mechanism on the opposite side of the moving magnet): active when dump is in beam position.
    ???
- Min 1 and 2: two switches, active when the motor arrives in the position with the moving magnet attached to the electro-magnet.
    ExpertAcquisition#sensorMotorIn1 / ExpertAcquisition#sensorMotorIn2
- Mout 1 and 2: two switches, active when the motor is in its parking position. The dump can be used.
    ExpertAcquisition#sensorMotorOut1 / ExpertAcquisition#sensorMotorOut2
```

## Acquisition Objectives.

``` 
- Live monitoring of all temperature sensors (11). 
    Inlet water (x2), outlet water (x2); graphite 1, graphite 2, Cu 1, Cu 2, Downstream vacuum chamber (x2), Electro-magnet).
    The monitoring of the temperature sensors on the main unit (dipole) shall also be possible. 
    
    Acquisition#temperatureWaterDumpIn, Acquisition#temperatureWaterChamber1, Acquisition#temperatureWaterDumpOut, Acquisition#temperatureWaterChamber2, Acquisition#temperatureGraphiteCore1, Acquisition#temperatureGraphiteCore2, Acquisition#temperatureCuCrZrCore1, Acquisition#temperatureCuCrZrCore2, Acquisition#temperatureVacuumChamber1, Acquisition#temperatureVacuumChamber2, Acquisition#temperatureMagnet

- Live monitoring of the electro-magnet current.
    ExpertAcquisition#magnetPSU1Current + ExpertAcquisition#magnetPSU2Current + Ok ???
- Live monitoring of water flow rate for both the dump core and the chamber.
    1) PR.TDI47:EXPACQ_CHMBR_H20_FLOW_AN
    2) PR.TDI47:EXPACQ_CHMBR_H20_TEMP_AN
    3) PR.TDI47:EXPACQ_HEAD_H20_FLOW_AN
    4) PR.TDI47:EXPACQ_HEAD_H20_TEMP_AN
    5) PR.TDI48:EXPACQ_CHMBR_H20_FLOW
    6) PR.TDI48:EXPACQ_CHMBR_H20_TEMP
    7) PR.TDI48:EXPACQ_HEAD_H20_FLOW
    8) PR.TDI48:EXPACQ_HEAD_H20_TEMP

    We only have 2 water flow meters:
    Acquisition#waterFlow1 and Acquisition#waterFlow2 for flow (m3/h) and Acquisition#temperatureWaterFlowMeter1 and temperatureWaterFlowMeter2.

- Live acquisition of the vacuum level in the dump tank.
    1) PR.VPG45/PR penning before the dumps
    2) PR.VGP49/PR penning after the dumps
    3) PR50.VPI47/PR ion pump 
    4) PR50.VPI47A/PR ion pump
    5) PR50.VPI48/PR ion pump
    6) PR50.VPI48A/PR ion pump

    Probably PVSS values. To be checked.

- Monitoring of the ongoing dumped power.
     i.e. the amount of protons dumped per second, based on the ongoing supercycle and dump operational mode.
        1) PX.DUMP:INTENSITY_AT_BEAM_DUMP
        2) PX.DUMP:MAGNETIC_FIELD_AT_BEAM_DUMP
        3) PX.DUMP:TIME_AT_BEAM_DUMP
        4) PX.DUMP:DUMP_NAME

    To be clarified with MRO or BE/OP. We have Acquisition#actualBeamIntensity.

- Live counter giving the integrated intensity dumped for each dump.
    Probably Acquisition#actualBeamIntensity or Acquisition#averagedBeamIntensity

- Live counting of the amount of flips done by the two dumps: mechanical cycles (with and without beam) + beam cycles (cycle with beam dumping).
    With beam : Acquisition#totalBeamDumpCycles
    Total : Acquisition#totalDumpCycles

- Download of all above data over the past 24 hours and over longer time via the logged variables on Timber.
    Please use Timber...
```

**Alarms and Warnings**

```
- Dumped power
    ExpertAcquisition#dumpedIntensityWarning

- Flowmeter values
    Acquisition#waterFlow1Warning
    Acquisition#waterFlow2Warning
    Acquisition#waterFlow1BelowLimit
    Acquisition#waterFlow2BelowLimit

- Magnet temperature
    ExpertACquisition#magnetOverTemperature
- DSout, DSin, DSup set to OFF
    ExpertAcquisition#sensorDumpSwitchIn1Ok
    ExpertAcquisition#sensorDumpSwitchIn2Ok
    ExpertAcquisition#sensorDumpSwitchOut1Ok
    ExpertAcquisition#sensorDumpSwitchOut2Ok

    DSup ???

- Active Interlocks
    ExpertAcquisition#anyInterlockActive
```
