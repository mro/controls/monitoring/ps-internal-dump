# PS Internal Dump SVG

SVG Images of the PS Internal Dump created from the respective PNG Images of the mechanism provided by MRO.

# SVG Conversion Guidelines

The application used for the SVG conversion is Inkscape.

The steps are the following:

* Open the image (ie. png) in Inkscape.
* Choose Path -> Trace bitmap -> Choose edge detection -> Lower Threshold (ie. 0.1) -> On options' suppress speckles increase the size (ie. 150)

In the desired result we intent to obtain fewer nodes in the path by lowering the edge detection threshold and increasing the speckles size also.

* Choose Path again -> Break Apart

In this way we end up with several paths in objects which we can group ourselves.


# SVG Configuration Editing

https://jakearchibald.github.io/svgomg/
